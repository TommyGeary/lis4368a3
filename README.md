> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Tommy Geary

### Assignment 3 Requirements:

*Three Parts*

1. Create repositories for a3
2. Create database and forward engineer
3. Questions

#### README.md file should include the following items:

* A3 erd and sql links
* PNG of ERD

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of A3 ERD*:

![A3 ERD Screenshot](img/a3.png)



#### A3 Links:

*A3 ERD Link*
[A3 ERD Link](docs/a3.mwb)

*A3 SQL Link*
[A3 SQL Link](docs/a3.sql)
